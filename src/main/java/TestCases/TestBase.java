package TestCases;

import Common.Constant;
import Utils.Configuration;
import Utils.DriverUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public class TestBase {

    @BeforeMethod(alwaysRun = true)
    public void TestInitializeMethod() {
        // Start Chrome browser and maximize window
        System.setProperty("webdriver.chrome.driver", "src/test/resource/libs/chromedriver.exe");
        config.setPlatform("chrome");
        config.setTimeout(30000);
        Constant.DRIVER = DriverUtils.create(config);
        Constant.DRIVER.manage().window().maximize();
    }

    @AfterMethod(alwaysRun = true)
    public void TestCleanupMethod() {
        Constant.DRIVER.quit();
    }
    public Configuration config = new Configuration();
}
