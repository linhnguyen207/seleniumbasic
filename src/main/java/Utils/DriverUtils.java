package Utils;

import Driver.ChromeDriverFactory;
import org.openqa.selenium.WebDriver;

public class DriverUtils {

    /**
     * Sleep
     *
     * @param millis
     */
    public static void pause(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static WebDriver create(Configuration config){
        ChromeDriverFactory dr = new ChromeDriverFactory();
        return dr.create(config);
    }
}
