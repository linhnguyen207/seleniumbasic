package Utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.logigear.utils.SelaiumKeys;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import static com.logigear.Platform.Chrome;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class Configuration {
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public boolean isHeadless() {
        return headless;
    }

    public void setHeadless(boolean headless) {
        this.headless = headless;
    }

    public String getRemote() {
        return remote;
    }

    public void setRemote(String remote) {
        this.remote = remote;
    }

    public String getBrowserSize() {
        return browserSize;
    }

    public void setBrowserSize(String browserSize) {
        this.browserSize = browserSize;
    }

    public String getDriverVersion() {
        return driverVersion;
    }

    public void setDriverVersion(String driverVersion) {
        this.driverVersion = driverVersion;
    }

    public boolean isStartMaximized() {
        return startMaximized;
    }

    public void setStartMaximized(boolean startMaximized) {
        this.startMaximized = startMaximized;
    }

    public String getPageLoadStrategy() {
        return pageLoadStrategy;
    }

    public void setPageLoadStrategy(String pageLoadStrategy) {
        this.pageLoadStrategy = pageLoadStrategy;
    }

    public MutableCapabilities getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(MutableCapabilities capabilities) {
        this.capabilities = capabilities;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public long getPollingInterval() {
        return pollingInterval;
    }

    public void setPollingInterval(long pollingInterval) {
        this.pollingInterval = pollingInterval;
    }

    public boolean isClickViaJs() {
        return clickViaJs;
    }

    public void setClickViaJs(boolean clickViaJs) {
        this.clickViaJs = clickViaJs;
    }


    public boolean isRemote() {
        return this.remote != null && !isBlank(this.remote);
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public Configuration() {

    }

    String platform;
    boolean headless;
    String remote;
    String browserSize;
    String driverVersion;
    boolean startMaximized;
    String pageLoadStrategy;
    MutableCapabilities capabilities;
    String baseUrl;
    long timeout;
    long pollingInterval;
    boolean clickViaJs;
    String chromeOptions;

    /**
     * Set true if you want to initial the default value
     * @param initial
     */
    public Configuration(boolean initial) {
        if (initial) {
            this.platform = System.getProperty(SelaiumKeys.Platform, Chrome.name());
            this.headless = Boolean.parseBoolean(System.getProperty(SelaiumKeys.Headless, "false"));
            this.remote = System.getProperty(SelaiumKeys.Remote);
            this.browserSize = System.getProperty(SelaiumKeys.BrowserSize, "1366x768");
            this.driverVersion = System.getProperty(SelaiumKeys.DriverVersion);
            this.startMaximized = Boolean.parseBoolean(System.getProperty(SelaiumKeys.StartMaximized, "false"));
            this.pageLoadStrategy = System.getProperty(SelaiumKeys.PageLoadStrategy, "normal");
            this.capabilities = new DesiredCapabilities();
            this.baseUrl = System.getProperty(SelaiumKeys.BaseUrl, "http://localhost:8080");
            this.timeout = Long.parseLong(System.getProperty(SelaiumKeys.Timeout, "4000"));
            this.pollingInterval = Long.parseLong(System.getProperty(SelaiumKeys.PollingInterval, "200"));
            this.clickViaJs = Boolean.parseBoolean(System.getProperty(SelaiumKeys.ClickViaJs, "false"));
        }
    }
}
