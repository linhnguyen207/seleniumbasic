package PageObjects;

import Common.Constant;
import Element.Element;
import Utils.DriverUtils;

public class HomePage extends GeneralPage {

    public void open(String url) {
        Constant.DRIVER.navigate().to(url);
    }

    public void enterkeyWordToSearch(String key){
        searchTextBox.enter(key);
        DriverUtils.pause(3000);
        resultLabel.setDynamicLocator(key);
        resultLabel.click();
    }

    private Element searchTextBox = new Element("//input[@name='q']");
    private Element resultLabel = new Element("//span[text()='%s']");
}
