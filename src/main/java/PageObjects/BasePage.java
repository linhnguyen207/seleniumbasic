package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Common.Constant;


public abstract class BasePage  {

	protected void SwitchWindow(int index) {
		Constant.DRIVER.switchTo().window(
				Constant.DRIVER.getWindowHandles().toArray()[index]
						.toString());
	}

	protected void CloseWindow() {
		Constant.DRIVER.close();
		if (Constant.DRIVER.getWindowHandles().size() > 0)
			SwitchWindow(0);
	}
	
	protected WebElement getElement(By by)
	{
		return Constant.DRIVER.findElement(by);
	}

	
	
}
