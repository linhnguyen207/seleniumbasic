package PageObjects;

import org.openqa.selenium.By;

public class LoginPage extends GeneralPage {

	static final By _txtUsername = By.xpath("//input[@id='username']");
	static final By _txtPassword = By.xpath("//input[@id='password']");
	static final By _btnLogin = By.xpath("//input[@value='login']");
	static final By _lblLoginErrorMsg = By
			.xpath("//p[@class='message error LoginForm']");

	public HomePage Login(String username, String password) {
		SubmitLoginCredentails(username, password);
		// Land on Home page
		return new HomePage();
	}

	public LoginPage LoginExpectingError(String username, String password) {
		SubmitLoginCredentails(username, password);
		return this;
	}

	private void SubmitLoginCredentails(String username, String password) {
		// Submit login credentails
		getElement(_txtUsername).sendKeys(username);
		getElement(_txtPassword).sendKeys(password);

		getElement(_btnLogin).click();

	}

}
