package Common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonMethods {

	public static String GenerateRandomString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		return dateFormat.format(date);		
	}

	public static String GenerateRandomEmail(String domain) {
		return GenerateRandomString() + "@" + domain;
	}



}
