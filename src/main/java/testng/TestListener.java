package testng;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class TestListener implements ITestListener {

    @Override
    public void onStart(ITestContext context) {
        log.info("Tests started on {}", context.getCurrentXmlTest().getParameter("platform"));
    }

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test case \"{} - {}\" is started", result.getMethod().getMethodName(),
                result.getMethod().getDescription());

    }

    @Override
    public void onFinish(ITestContext context) {

    }

    @Override
    public void onTestFailure(ITestResult result) {
//        String className = String.format(result.getTestClass().getName());
//        log.info("Field with name driver  of myClass: " + className);
//        String driver="";
//        try {
//            Class<?> clzz  = Class.forName(className);
//            Object obj = clzz.newInstance();
//            Field field = clzz.getField("driverID");
//            driver  = field.get(obj).toString();
//            log.info("Driver " + driver);
//            // call action check message
//        } catch (ClassNotFoundException | IllegalAccessException | NoSuchFieldException | InstantiationException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onTestSuccess(ITestResult result) {

    }
    private static final Logger log = LoggerFactory.getLogger(TestListener.class);

}

