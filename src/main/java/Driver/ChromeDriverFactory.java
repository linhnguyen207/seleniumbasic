package Driver;

import Utils.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.regex.Matcher.quoteReplacement;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ChromeDriverFactory  {

    public WebDriver create(Configuration config) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(config.isHeadless());
        chromeOptions.addArguments(createChromeArguments());
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("pageLoadStrategy", config.getPageLoadStrategy());
        capabilities.setCapability("acceptSslCerts", true);
        //Handle popup save password
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        chromeOptions.setExperimentalOption("prefs", prefs);

        //Disabled Popup when running script
        HashMap<String, Object> chromeLocalStatePrefs = new HashMap<String, Object>();
        List<String> experimentalFlags = new ArrayList<String>();
        experimentalFlags.add("disable-popup-blocking");
        chromeLocalStatePrefs.put("browser.enabled_labs_experiments", experimentalFlags);
        chromeOptions.setExperimentalOption("localState", chromeLocalStatePrefs);
        chromeOptions.setExperimentalOption("excludeSwitches", excludeSwitches());
        //end

        chromeOptions.merge(capabilities);
        log.debug("Chrome options: {}", chromeOptions);
        if (config.isRemote()) {
            try {
                RemoteWebDriver webDriver = new RemoteWebDriver(new URL(config.getRemote()), chromeOptions);
                webDriver.setFileDetector(new LocalFileDetector());
                return webDriver;
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException("Invalid 'remote' parameter: " + config.getRemote(), e);
            }
        } else {
            String driverVersion = config.getDriverVersion();
            WebDriverManager.chromedriver().driverVersion(driverVersion).setup();
            return new ChromeDriver(chromeOptions);
        }
    }

    private String[] excludeSwitches() {
        return new String[]{"enable-automation", "load-extension"};
    }

    protected List<String> createChromeArguments() {
        List<String> arguments = new ArrayList<>();
        arguments.add("--disable-dev-shm-usage");
        arguments.add("--start-maximized");
        arguments.addAll(parseArguments(System.getProperty("chromeOptions.args")));
        return arguments;
    }

    private List<String> parseArguments(String arguments) {
        return parseCSV(arguments).stream()
                .map(this::removeQuotes)
                .collect(toList());
    }

    private String removeQuotes(String value) {
        return REGEX_REMOVE_QUOTES.matcher(value).replaceAll(quoteReplacement(""));
    }

    /**
     * parse parameters which can come from command-line interface
     *
     * @param csvString comma-separated values, quotes can be used to mask spaces and commas
     *                  Example: 123,"foo bar","bar,foo"
     * @return values as array, quotes are preserved
     */
    final List<String> parseCSV(String csvString) {
        return isBlank(csvString) ? emptyList() : asList(REGEX_COMMAS_IN_VALUES.split(csvString));
    }

    private static final Logger log = LoggerFactory.getLogger(ChromeDriverFactory.class);
    // Regexp from https://stackoverflow.com/a/15739087/1110503 to handle commas in values
    private static final Pattern REGEX_COMMAS_IN_VALUES = Pattern.compile(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
    private static final Pattern REGEX_REMOVE_QUOTES = Pattern.compile("\"", Pattern.LITERAL);

}
