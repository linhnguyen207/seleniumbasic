package Element;

import Common.Constant;
import com.logigear.element.BaseElement;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Element {
    private static final Logger log = LoggerFactory.getLogger(BaseElement.class);
    protected String locator;
    protected By by;
    protected String locatorType;
    protected String locatorValue;
    protected String dynamicLocator;
    protected boolean alwaysFind = false;
    public Element(String locator) {
        this.locator = locator;
        this.dynamicLocator = locator;
        this.by = by();
    }

    protected Element(String locator, boolean alwaysFind) {
        this(locator);
        this.alwaysFind = alwaysFind;
    }

    protected Element(By by) {
        this.by = by;
    }

    public String getLocator() {
        return locator;
    }

    protected By by() {
        this.locatorValue = this.locator.replaceAll("[\\w\\s]*=(.*)", "$1").trim();
        String type = this.locator.replaceAll("([\\w\\s]*)=.*", "$1").trim();
        this.locatorType = type;
        switch (type) {
            case "css":
                return By.cssSelector(this.locatorValue);
            case "id":
                return By.id(this.locatorValue);
            case "link":
                return By.linkText(this.locatorValue);
            case "xpath":
                return By.xpath(this.locatorValue);
            case "text":
                return By.xpath(String.format("//*[contains(text(), '%s')]", this.locatorValue));
            case "name":
                return By.name(this.locatorValue);
            case "accid":
                return MobileBy.AccessibilityId(this.locatorValue);
            case "iosclschain":
                return MobileBy.iOSClassChain(this.locatorValue);
            case "iospredicate":
                return MobileBy.iOSNsPredicateString(this.locatorValue);
            case "uia":
                return MobileBy.AndroidUIAutomator(this.locatorValue);
            case "className":
                return By.className(this.locatorValue);
            default:
                this.locatorValue = locator;
                return By.xpath(this.locatorValue);
        }
    }

    protected WebDriver webDriver() {
        return Constant.DRIVER;
    }
    public void setDynamicLocator(Object... args) {
        this.locator = String.format(this.dynamicLocator, args);
        this.by = by();
    }
    public void click() {
        try {
            webDriver().findElement(this.by()).click();
        }catch (WebDriverException e){
            e.getMessage();
        }
    }

    public void enter(CharSequence... value) {
        try {
            webDriver().findElement(this.by()).sendKeys(value);
        }catch (WebDriverException e){
            e.getMessage();
        }
    }

}
